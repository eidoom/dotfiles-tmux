# dotfiles-tmux

## Description

My installation script and configuration files for `tmux`.

## Usage

### Installation

#### Manual installation

* Install with 
    * SSH clone (if you're me)
    ```shell
    git clone --recurse-submodules git@gitlab.com:eidoom/dotfiles-tmux.git
    ```
    * or HTTPS clone (if you're not me)
    ```shell
    git clone --depth 1 --recurse-submodules https://gitlab.com/eidoom/dotfiles-tmux.git
    ```
* then run installation script
```shell
cd dotfiles-tmux
./install.sh
```
* then in `tmux`, `prefix`+`I` to load plugins.

#### Superproject

* Alternatively, install as part of the [superproject](https://gitlab.com/eidoom/dotfiles-public).

### Configuration

* Set machine specific settings in `~/.tmux/tmux-private.conf`

## Distro support

Installation of system packages in the installation script is probably specific to tested distributions.

Tested on:
* Fedora 31
* Debian 10
