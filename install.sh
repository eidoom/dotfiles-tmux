#!/usr/bin/env bash

here=$(realpath "$0" | xargs dirname)
cd "$here" || exit
base_conf=tmux.conf
files=(.$base_conf .tmux/plugins/tpm)

if [[ $(grep PRETTY_NAME /etc/os-release | sed 's/PRETTY_NAME="\(\w\+\).*"/\1/g') == Fedora ]]; then
    sudo dnf install -y tmux tmux-powerline
    echo -e "# Load powerline\nsource /usr/share/tmux/powerline.conf\n\n$(cat $base_conf)" > .$base_conf
elif [[ $(grep PRETTY_NAME /etc/os-release | sed 's/PRETTY_NAME="\(\w\+\).*"/\1/g') == Debian ]]; then
    sudo apt install -y tmux powerline
    echo -e "# Load powerline\nsource /usr/share/powerline/bindings/tmux/powerline.conf\n\n$(cat $base_conf)" > .$base_conf
fi

log="$here/install.log"
touch "$log"

for file in ${files[@]}; do
    if [[ $file == *"/"* ]]; then
        mkdir -p ~/"${file%/*}"
    fi
    link="$HOME/$file"
    ln -s "$here/$file" "$link"
    if grep -Fxq "$link" "$log"; then
        echo "$link previously installed"
    else
        echo "Logging installation of symlink $link to $log"
        echo "$link" >>"$log"
    fi
done

touch $HOME/.tmux/tmux-private.conf
